import React from 'react'
import Booklist from '../components/Booklist'

const Books = () => {
  return (
    <>
      <Booklist />
    </>
  )
}

export default Books
