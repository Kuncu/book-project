import React, { useState, useEffect } from 'react'

const Booklist = () => {
  let books = JSON.parse(localStorage.getItem('list'))
  const [book, setBook] = useState(books)

  useEffect(() => {
    localStorage.getItem('list')
  }, [])

  return (
    <div className='container mx-auto'>
      <div className='mt-20 flex justify-center items-center'>
        {book.map((item) => {
          return (
            <div key={item.id} className='p-2 m-2 bg-yellow-100 w-1/4'>
              <div className='p-1'>
                <h5 className='font-semibold  '>Book Name</h5>
                <h3>{item.title}</h3>
              </div>
              <div className='p-1'>
                <h5 className='font-semibold  '>Writer Name</h5>
                <h3>{item.writer}</h3>
              </div>
              <div className='p-1'>
                <h5 className='font-semibold  '>Total Page</h5>
                <h3>{item.pageCount}</h3>
              </div>
            </div>
          )
        })}
      </div>
    </div>
  )
}

export default Booklist
