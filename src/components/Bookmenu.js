import React from 'react'
import { Link } from 'react-router-dom'

const Bookmenu = () => {
  return (
    <div className='p-2 flex justify-center'>
      <Link
        to='/'
        className='bg-blue-500 text-white px-6 py-3 rounded-full mx-2'
      >
        Create Book
      </Link>
      <Link
        to='/books'
        className='bg-blue-500 text-white px-6 py-3 rounded-full mx-2'
      >
        Show Books
      </Link>
    </div>
  )
}

export default Bookmenu
