import React, { useState, useEffect } from 'react'

const getLocalStorage = () => {
  let list = localStorage.getItem('list')
  if (list) {
    return JSON.parse(localStorage.getItem('list'))
  } else {
    return []
  }
}

const Createtab = () => {
  const [list, setList] = useState(getLocalStorage())
  const [bookName, setBookName] = useState('')
  const [writerName, setWriterName] = useState('')
  const [pageNumber, setPageNumber] = useState(0)
  const [info, setInfo] = useState('')

  const handleSubmit = (e) => {
    e.preventDefault()
    if (!bookName || !writerName) {
      setInfo('Please fill the blanks')
    } else {
      const newBook = {
        id: new Date().getTime().toString(),
        title: bookName,
        writer: writerName,
        pageCount: pageNumber,
      }
      setList([...list, newBook])

      setBookName('')
      setWriterName('')
      setInfo('Book created')
    }
  }
  useEffect(() => {
    localStorage.setItem('list', JSON.stringify(list))
  }, [list])

  return (
    <div className='flex justify-center items-center mt-20'>
      <div className='bg-red-200 px-10 rounded-lg '>
        <div className='p-3 text-center'>
          <h6>Enter Book Name</h6>
          <input
            value={bookName}
            onChange={(e) => setBookName(e.target.value)}
            className='rounded-md'
            type='text'
            placeholder='Book Name'
          />
        </div>
        <div className='p-3 text-center'>
          <h6>Enter Writer Name</h6>
          <input
            value={writerName}
            onChange={(e) => setWriterName(e.target.value)}
            className='rounded-md'
            type='text'
            placeholder='Writer Name'
          />
        </div>
        <div className='p-3 text-center'>
          <h6>Enter Total Page Number </h6>
          <input
            value={pageNumber}
            onChange={(e) => setPageNumber(e.target.value)}
            className='rounded-md'
            type='number'
            placeholder='Page Number'
          />
        </div>
        <div className='flex justify-center p-3'>
          <button
            onClick={handleSubmit}
            className='bg-blue-500 py-3 px-6 rounded-full text-white'
          >
            Submit
          </button>
        </div>
        <div className='p-3 text-center'>
          <h3>{info}</h3>
        </div>
      </div>
    </div>
  )
}

export default Createtab
