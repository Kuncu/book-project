import { Route, Switch } from 'react-router'
import Bookmenu from './components/Bookmenu'
import Home from './pages'
import Books from './pages/books'

function App() {
  return (
    <>
      <div className='container mx-auto'>
        <Bookmenu />
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/books' component={Books} />
        </Switch>
      </div>
    </>
  )
}

export default App
